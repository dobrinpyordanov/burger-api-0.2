import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { BurgerEntity } from "src/entity/Burger";
import { BurgersService } from "./burgers.service";
import { BurgersController } from "./burgers.controller";

@Module({
  imports: [TypeOrmModule.forFeature([BurgerEntity])],
  controllers: [BurgersController],
  providers: [BurgersService],
})
export class BurgersModule {}