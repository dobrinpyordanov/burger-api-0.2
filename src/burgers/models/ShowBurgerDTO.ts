import { Expose } from "class-transformer";

export class ShowBurgerDTO {
  @Expose()
  public id: string;
  @Expose()
  public name: string;
  @Expose()
  public ingredients: string;
  @Expose()
  public image_url: string;
  @Expose()
  public size: string;
}