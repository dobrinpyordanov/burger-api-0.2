import { Length, Matches } from 'class-validator';

export class CreateBurgerDTO {
  @Length(5, 40)
  public name: string;
  @Length(5, 200)
  public ingredients: string;
  @Matches(/(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)/)
  public image_url: string;
  @Length(1,20)
  public size: string;
}
