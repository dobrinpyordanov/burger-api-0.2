import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Body,
  Param,
  ValidationPipe,
} from '@nestjs/common';
import { BurgersService } from './burgers.service';
import { ShowBurgerDTO } from './models/ShowBurgerDTO';
import { CreateBurgerDTO } from './models/CreateBurgerDTO';


@Controller('api')
export class BurgersController {
  public constructor(private readonly burgersService: BurgersService) { }

  @Get('/burgers')
  @HttpCode(HttpStatus.OK)
  async getAllBurgers(): Promise<ShowBurgerDTO[]> {
    return await this.burgersService.getAllBurgers();
  }

  @Get('/burgers/:id')
  @HttpCode(HttpStatus.OK)
  async getBurgerById(@Param('id') id: string): Promise<ShowBurgerDTO> {
    return await this.burgersService.getBurgerById(id);
  }
  
  @Get('/random')
  @HttpCode(HttpStatus.OK)
  async getRandom(): Promise<ShowBurgerDTO> {
    return await this.burgersService.getRandom();
  }

  @Post('/burgers')
  @HttpCode(HttpStatus.CREATED)
  async addBurger( @Body(new ValidationPipe({ whitelist: true })) burger: CreateBurgerDTO): Promise<ShowBurgerDTO> {
    return await this.burgersService.createBurger(burger);
  }
}
