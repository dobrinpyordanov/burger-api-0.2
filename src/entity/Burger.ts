import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('burger')
export class BurgerEntity {

  @PrimaryGeneratedColumn()
  id: string;

  @Column('nvarchar')
  name: string;

  @Column()
  ingredients: string;

  @Column()
  image_url: string;

  @Column()
  size: string;

  @Column({ default: false })
  isDeleted: boolean;

}
